(function chooseDeviceMechanism (){

    var mechanismButton,
        chooseDeviceMechanism,
        brands,
        showSection,
        mechanismSection,
        oldSectionHeigh;

    mechanismButton = document.querySelector('.wrap .choose-device-mechanism .choose-device'),
    chooseDeviceMechanism = document.querySelector('.wrap .choose-device-mechanism'),
    brands = document.querySelectorAll('.wrap .brand'),
    showSection = document.querySelector('.wrap .models-show'),
    mechanismSection = document.querySelector('.wrap .choose-mechanism');
    oldSectionHeigh = mechanismSection.style.height;

    window.addEventListener('ready', changeSectionHeight);
    window.addEventListener('resize', changeSectionHeight);

    function changeSectionHeight(){
        var cssMechanismSectionHeigh = parseInt(window.getComputedStyle(mechanismSection).getPropertyValue('height'), 10),
            cssShowSectionHeight = parseInt(window.getComputedStyle(showSection).getPropertyValue('height'), 10);

        if(cssShowSectionHeight > cssMechanismSectionHeigh) {
            mechanismSection.style.height = cssShowSectionHeight + 40 + 'px';
        } else {
            showSection.style.minHeight = '86%';
            //mechanismSection.style.height = oldSectionHeigh;
        }
    }

   function showCategory(cat){
       var clone = cat.cloneNode(true);
       showSection.innerHTML = '';
       showSection.appendChild(clone);
       changeSectionHeight();
   }

   function chooseBrand(event){
       var element = this,
           categories = element.querySelectorAll('.brand-models'),
           siblings = element.parentElement.querySelectorAll('.brand');

       changeSectionHeight();

       for(var i=0; i < siblings.length; i++){
           siblings[i].classList.remove('brand-active');
       }

       element.classList.add('brand-active');

      for(var i = 0; i < categories.length; i++){
          showCategory(categories[i]);
      }
   }

   for(var i = 0; i < brands.length; i++){
      var brand = brands[i];
      brand.addEventListener('click', chooseBrand);
   }

    brands[0].click();

   mechanismButton.addEventListener('click', function (event) {
      chooseDeviceMechanism.classList.toggle('choose-device-mechanism-activate');
   });
})();