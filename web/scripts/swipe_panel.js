(function(){
    var slideout = new Slideout({
        'panel': document.querySelector('.wrap'),
        'menu': document.querySelector('#swipe-menu'),
        'padding': 256,
        'tolerance': 70,
        'side': 'right'
    });

    document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
    });
})()