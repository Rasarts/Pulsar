(function () {
    var brands = document.querySelectorAll('#swipe-menu .brand'),
        chooseSwipe = document.querySelector('#swipe-menu .choose-device-mechanism-swipe');

    chooseSwipe.addEventListener('click', function (event) {
        event.stopPropagation();
        this.classList.toggle('choose-device-mechanism-swipe-activate');
    });

    function classToggle(element) {
        var siblings = element.parentElement.querySelectorAll('.brand');
        for (var i = 0; i < siblings.length; i++) {
            siblings[i].classList.remove('brand-active');
        }
        element.classList.toggle('brand-active');
    }

    for (var i = 0; i < brands.length; i++) {
        brands[i].addEventListener('click', function (event) {
            event.stopPropagation();
            var element = this;
            classToggle(element);
        });
    }
})();